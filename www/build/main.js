webpackJsonp([4],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


/*component */
var DetailsPage = (function () {
    function DetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.item = navParams.data.item;
    }
    /*ionViewLoad */
    DetailsPage.prototype.ionViewDidLoad = function () {
        console.log('Hello DetailsPage Page');
    };
    return DetailsPage;
}());
DetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-details',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/details/details.html"*/'\n\n<ion-header>\n  <ion-navbar >\n    <ion-title class="opnav">\n       <!--tile of the page--> \n     {{item.Name}}\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n \n<ion-content>\n\n  <ion-list class="accordion-list">\n    \n     <ion-item  text-wrap>\n         <!--dislplaying items for detail page of CF and niutrition guide--> \n             <ion-card-title>\n        {{item.Name}}\n      </ion-card-title>\n  <ion-card>\n    <img src="assets/img/{{item.Image}}"/>\n    \n  </ion-card>\n \n     \n      <p class="pline">\n        {{item.Design}}\n      </p>\n    \n \n            \n          </ion-item>\n    \n  </ion-list>\n  \n\n</ion-content>\n\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/details/details.html"*/
    })
    /*page class */
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], DetailsPage);

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WhmviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


//ionic page function
var WhmviewPage = (function () {
    function WhmviewPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    //load data to view items details
    WhmviewPage.prototype.ionViewDidLoad = function () {
        this.Height = this.navParams.get('item').Height;
        this.Weight = this.navParams.get('item').Weight;
        this.myDate = this.navParams.get('item').myDate;
    };
    return WhmviewPage;
}());
WhmviewPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-whmview',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/whmview/whmview.html"*/'\n<!--page for weight and height view data  -->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="opnav">Details</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<!--page contents -->\n\n<ion-content class="monitor">\n  \n      <ion-card>\n            <!--Displaying details -->\n          <ion-card-content>\n              <p><b>Height & Weight on:</b> {{myDate}}</p>\n              <br>\n              <p><b></b>Height= {{Height}} CM</p>\n                <p><b></b> Weight = {{Weight}} KG</p>\n               \n          </ion-card-content>\n  \n          <ion-list>\n             \n                 \n \n          </ion-list>\n  \n      </ion-card>\n  \n  </ion-content>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/whmview/whmview.html"*/,
    })
    //export class of the page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], WhmviewPage);

//# sourceMappingURL=whmview.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodvPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


// food items view detail page
var FoodvPage = (function () {
    function FoodvPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    //ionviewdidload function
    FoodvPage.prototype.ionViewDidLoad = function () {
        this.Dname = this.navParams.get('item').Dname;
        this.Time = this.navParams.get('item').Time;
        this.myDate = this.navParams.get('item').myDate;
    };
    return FoodvPage;
}());
FoodvPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-foodv',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/foodv/foodv.html"*/'\n\n  <!--detal of dietary monitoring-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="opnav">Details</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="monitor">\n  \n      <ion-card>\n           <!--getting data and displaying dietary monitoring info-->\n          <ion-card-content>\n              <p><b>Dietary on:</b> {{myDate}}</p>\n              <br>\n              <p><b></b>Name: {{Dname}} </p>\n                <p><b></b> Time : {{Time}}</p>\n               \n          </ion-card-content>\n  \n          <ion-list>\n             \n                 \n \n          </ion-list>\n  \n      </ion-card>\n  \n  </ion-content>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/foodv/foodv.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], FoodvPage);

//# sourceMappingURL=foodv.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodaddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


//add diatary items page
var FoodaddPage = (function () {
    function FoodaddPage(navCtrl, navParams, view) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
    }
    //save item function
    FoodaddPage.prototype.saveItem = function () {
        var newItem = {
            myDate: this.myDate,
            Dname: this.Dname,
            Time: this.Time,
        };
        //view dismiss function
        this.view.dismiss(newItem);
    };
    //close window fucntion
    FoodaddPage.prototype.close = function () {
        this.view.dismiss();
    };
    return FoodaddPage;
}());
FoodaddPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-foodadd',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/foodadd/foodadd.html"*/'\n<ion-header>\n  \n  <ion-navbar>\n    <ion-title>Data Entry</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-list>\n  \n    <!--Add food page items. input using ngModel -->\n<ion-item>    \n<ion-label floating>Dietary Name: </ion-label>\n <ion-input type="text" [(ngModel)]="Dname"></ion-input>\n</ion-item>\n<ion-item>    \n  <ion-label floating>Time</ion-label>\n   <ion-input type="Time" [(ngModel)]="Time"></ion-input>\n  </ion-item>\n\n  <ion-item>\n<ion-label floating>Date</ion-label>\n<ion-datetime displayFormat="MMM/DD/YYYY" [(ngModel)]="myDate"></ion-datetime>\n\n</ion-item>\n\n</ion-list>\n<button full ion-button color="danger" (click)="saveItem()">Save</button> \n\n</ion-content>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/foodadd/foodadd.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], FoodaddPage);

//# sourceMappingURL=foodadd.js.map

/***/ }),

/***/ 116:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 116;

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/foodadd/foodadd.module": [
		293,
		3
	],
	"../pages/foodv/foodv.module": [
		292,
		2
	],
	"../pages/selfcareguide/selfcareguide.module": [
		290,
		1
	],
	"../pages/whmview/whmview.module": [
		291,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 158;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__monitoring_monitoring__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_about__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cysfib_cysfib__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__self_care_self_care__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directory_directory__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */






//components page funcion
var TabsPage = (function () {
    function TabsPage() {
        //tab nav for the pages
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__cysfib_cysfib__["a" /* CysfibPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__monitoring_monitoring__["a" /* MonitoringPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__about_about__["a" /* AboutPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__self_care_self_care__["a" /* SelfcarePage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_5__directory_directory__["a" /* DirectoryPage */];
    }
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/tabs/tabs.html"*/'\n\n  <!--tab bar contents-->\n<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="Self-care" tabIcon="person"></ion-tab>    \n  <ion-tab [root]="tab2Root" tabTitle="Monitoring" tabIcon="calendar"></ion-tab>\n  <ion-tab [root]="tab5Root" tabTitle="Directory" tabIcon="contacts"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/tabs/tabs.html"*/
    })
    //export class of the page
    ,
    __metadata("design:paramtypes", [])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonitoringPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__healthcal_healthcal__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__medication_medication__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__whm_whm__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__food_food__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */






//component function
var MonitoringPage = (function () {
    //constructor
    function MonitoringPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    MonitoringPage.prototype.Healthcal = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__healthcal_healthcal__["a" /* HealthcalPage */], {
            val: 'Healthcal'
        });
    };
    //Dily activity page nav
    MonitoringPage.prototype.DailyAct = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__medication_medication__["a" /* MedicationPage */], {
            val: 'DailyAct'
        });
    };
    //Weight and height monitoring page nav
    MonitoringPage.prototype.Todo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__whm_whm__["a" /* WhmPage */], {
            val: 'Todo'
        });
    };
    //Diatary management page nav
    MonitoringPage.prototype.Fod = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__food_food__["a" /* FoodPage */], {
            val: ' FoodPage'
        });
    };
    return MonitoringPage;
}());
MonitoringPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'monitoring',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/monitoring/monitoring.html"*/'\n\n  <!--monitoring page-->\n<ion-header>\n  <ion-navbar>\n    <ion-title  >\n      Self care\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n  <!--navigation section of the page-->\n<ion-content padding>\n  <ion-list>\n    <ion-list-header class="ggh2">\n       <h2 class="ggh2h" > Monitoring </h2>\n    </ion-list-header>\n        <!--Helath calculator page navigation -->\n      <ion-item (click)="Healthcal()">\n        <ion-avatar item-start>\n        <ion-icon clear item-right name="ios-pulse" class="debutton"></ion-icon>\n    </ion-avatar>\n        <h2>Health calculator</h2>  \n        <p class="scfp">Measure your health daily</p>\n     <ion-icon clear item-right name="arrow-forward" class="debutton"></ion-icon>\n  </ion-item>\n\n   \n</ion-list>\n\n      <!--Medication monitor page navigation -->\n<ion-list>\n<ion-item (click)="DailyAct()">\n        <ion-avatar item-start>\n        <ion-icon clear item-right name="ios-analytics" class="debutton"></ion-icon>\n    </ion-avatar>\n        <h2>Monitor Medication</h2>  \n        <p class="scfp">Manage and monitor daily medication</p>\n     <ion-icon clear item-right name="arrow-forward" class="debutton"></ion-icon>\n  </ion-item>\n</ion-list>\n      <!--Dietary Monitoring page navigation -->\n<ion-list>\n<ion-item (click)="Fod()">\n        <ion-avatar item-start>\n        <ion-icon clear item-right name="nutrition" class="debutton"></ion-icon>\n    </ion-avatar>\n        <h2>Monitor Dietary</h2>  \n        <p class="scfp">Manage and monitor dietary</p>\n     <ion-icon clear item-right name="arrow-forward" class="debutton"></ion-icon>\n  </ion-item>\n</ion-list>\n      <!--Height and weight monitoring page navigation -->\n<ion-list>\n<ion-item (click)="Todo()">\n        <ion-avatar item-start>\n        <ion-icon clear item-right name="list" class="debutton"></ion-icon>\n    </ion-avatar>\n        <h2>Height and Weight Monitoring</h2>  \n        <p class="scfp">Monitor Health and Weight</p>\n     <ion-icon clear item-right name="arrow-forward" class="debutton"></ion-icon>\n  </ion-item>\n</ion-list>\n\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/monitoring/monitoring.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
], MonitoringPage);

//# sourceMappingURL=monitoring.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthcalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nutrition_nutrition__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__selfcareguide_selfcareguide__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cfg_cfg__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */






//health calculator page ts file
var HealthcalPage = (function () {
    //construction
    function HealthcalPage(navCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    //addthefunction is fo the the calculation .. fomular used to get the health result is x=Height/(weight)^2
    HealthcalPage.prototype.addThemFunction = function () {
        var firstNumber = this.firstNumber ? parseFloat(this.firstNumber) : 0;
        var secondNumber = this.secondNumber ? parseFloat(this.secondNumber) : 0;
        var mpm = firstNumber / (secondNumber * (secondNumber));
        //if condition to catagorise for each result
        if (0.0346 < mpm && mpm < 0.0561) {
            this.total = 'Normal weight : Please maintain your diatary guidelines';
            console.log("clicked!");
            this.view = 'n';
        }
        else if (0.0346 > mpm) {
            this.total = 'Over weighted: Please Follow the Diatary Guidelines, exercises and loose weight ';
            console.log("clicked!");
            this.view = 'o';
        }
        else if (mpm > 0.0561) {
            this.total = 'Under weight: Please Follow the Diatary Guidelines and Gain More weight :';
            console.log("clicked!");
            this.view = 'u';
        }
    };
    //get instant total
    HealthcalPage.prototype.getInstantTotal = function () {
        var firstNumber = this.firstNumber ? parseFloat(this.firstNumber) : 0;
        var secondNumber = this.secondNumber ? parseFloat(this.secondNumber) : 0;
        var mpm = firstNumber + secondNumber;
        if (mpm === 2) {
            return firstNumber + secondNumber;
        }
        //load1 for navigation
    };
    HealthcalPage.prototype.load1 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__nutrition_nutrition__["a" /* NutritionPage */], {
            val: 'Nutrition'
        });
    };
    //navigation function to go self care guide page
    HealthcalPage.prototype.loadSelfcareguide = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__selfcareguide_selfcareguide__["a" /* SelfcareguidePage */], {
            val: 'Selfcareguide'
        });
    };
    //navigation function to go CF guide page
    HealthcalPage.prototype.loadCfg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cfg_cfg__["a" /* CfgPage */], {
            val: 'Cfg'
        });
    };
    return HealthcalPage;
}());
HealthcalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'healthcal',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/healthcal/healthcal.html"*/'\n\n  <!--Health calculation page-->\n<ion-header>\n\n    <ion-navbar>\n\n        <ion-title class="opnav">\n\n            Health Calculator\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n <ion-list-header class="ggh2">\n     <h2 class="ggh2h"> Health Calculator </h2>\n    </ion-list-header> \n</ion-list>\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label color="dark">Height: </ion-label>\n                <!--Height imput in Health calculation-->\n                <ion-input type="number" [(ngModel)]="firstNumber" placeholder="Enter height (CM)"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n            <!--Weight imput in Health calculation-->\n            <ion-label color="dark">Weight: </ion-label>\n\n            <ion-input type="number" [(ngModel)]="secondNumber" placeholder="Enter weight (KG)"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n     <ion-row>\n    \n    <ion-col center text-center>\n      <button ion-button block color="danger"(click)="addThemFunction()">Calculate</button>\n    </ion-col>\n  </ion-row>\n\n\n    <ion-card>\n\n      \n         <!--Display results and suggestion, using ngIF for different conditions-->\n        <ion-card-content center text-center *ngIf=" view == \'u\'">\n       <p *ngIf=" view == \'u\'" > <b> Your Health Results:</b><br> {{total}} <a (click)="load1()">Nutrition</a>, <a (click)="loadSelfcareguide()">Self-care guide</a> and <a (click)="loadCfg()">CF Guide</a>    </p> \n        </ion-card-content>\n        <ion-card-content center text-center *ngIf=" view == \'o\'">\n        <p *ngIf=" view == \'o\'" > <b> Your Health Results:</b><br> {{total}} <a (click)="load1()">Nutrition</a>, <a (click)="loadSelfcareguide()">Self-care guide</a> and <a (click)="loadCfg()">CF Guide</a>    </p> \n         </ion-card-content>\n        <ion-card-content center text-center *ngIf=" view == \'n\'">         \n         <p *ngIf=" view == \'n\'" > <b> Your Health Results:</b> <br>{{total}} <a (click)="load1()">Nutrition</a>, <a (click)="loadSelfcareguide()">Self-care guide</a> and <a (click)="loadCfg()">CF Guide</a>    </p> \n        </ion-card-content>\n\n\n    </ion-card>\n \n\n</ion-content>\n\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/healthcal/healthcal.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
], HealthcalPage);

//# sourceMappingURL=healthcal.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MedicationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__medicationadd_medicationadd__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__medicationview_medicationview__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_dailyact_dailyact__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */





/*
component function
*/
var MedicationPage = (function () {
    //contructor
    function MedicationPage(navCtrl, navParams, modalCtrl, dataService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.dataService = dataService;
        //array for the medication items 
        this.itemsA = [];
        this.dataService.getData().then(function (dailyacts) {
            if (dailyacts) {
                //console.log(todos)
                _this.itemsA = JSON.parse(dailyacts);
            }
        });
    }
    //view load function
    MedicationPage.prototype.ionViewDidLoad = function () {
    };
    MedicationPage.prototype.addItem = function () {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_1__medicationadd_medicationadd__["a" /* MedicationaddPage */]);
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.saveItem(item);
            }
        });
        addModal.present();
    };
    //save data function
    MedicationPage.prototype.saveItem = function (item) {
        this.itemsA.push(item);
        this.dataService.save(this.itemsA);
    };
    //view data function
    MedicationPage.prototype.viewItem = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__medicationview_medicationview__["a" /* MedicationviewPage */], {
            item: item
        });
    };
    //delete data function
    MedicationPage.prototype.delete = function (index) {
        this.itemsA.splice(index, 1);
        this.dataService.save(this.itemsA);
    };
    return MedicationPage;
}());
MedicationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/medication/medication.html"*/'\n\n  <!--Medication calculation page-->\n<ion-header>\n  \n    <ion-navbar>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Monitor Medication</ion-title>\n          <ion-buttons start>\n              <!--add button to call add medicaiton page-->\n            <button ion-button color="primary"   id="addmx2" icon-only (click)="addItem()"><ion-icon ios="ios-add" md="md-add" id="addmx"></ion-icon></button>\n           \n            </ion-buttons>\n      </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content>\n       <!--details of medicaiton page-->\n    <ion-list detail-non>\n      <ion-item-sliding  *ngFor="let item of itemsA; let i = index"(click)="viewItem(item)" detail-none> \n        <ion-item >\n           <ion-icon name= "arrow-forward" item-right></ion-icon>\n           <p> <b>Med. Name : </b> {{item.Mname}}    | <b>   Milligram : </b> {{item.Mil}} MG</p>\n           <p>  <b>Frequency : </b> {{item.Freq}}    | <b> Date :</b> {{item.myDate}}  </p>\n\n        \n   <!--delete part of medicaiton page-->\n       </ion-item>\n       <ion-item-options side="left">\n         <button ion-button color="danger" (click)="delete(i)">delete</button>\n       </ion-item-options>\n   \n   \n     </ion-item-sliding>\n     <p>   slide right to delete. click to view item details</p>\n  </ion-list>\n  </ion-content>\n  '/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/medication/medication.html"*/,
    })
    //clss for the page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__providers_dailyact_dailyact__["a" /* DailyactProvider */]])
], MedicationPage);

//# sourceMappingURL=medication.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MedicationaddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


/*
compoenent function
*/
var MedicationaddPage = (function () {
    function MedicationaddPage(navCtrl, navParams, view) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
    }
    //save item function
    MedicationaddPage.prototype.saveItem = function () {
        var newItem = {
            myDate: this.myDate,
            Mname: this.Mname,
            Mil: this.Mil,
            Freq: this.Freq,
        };
        this.view.dismiss(newItem);
    };
    //closs function
    MedicationaddPage.prototype.close = function () {
        this.view.dismiss();
    };
    return MedicationaddPage;
}());
MedicationaddPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/medicationadd/medicationadd.html"*/'\n\n  <!--Medication add page-->\n<ion-header>\n  \n  <ion-navbar>\n    <ion-title>Data Entry</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-list>\n  \n\n  <!--Medication add page contents-->\n<ion-item>    \n<ion-label floating>Med. Name</ion-label>\n <ion-input type="text" [(ngModel)]="Mname"></ion-input>\n</ion-item>\n<ion-item>    \n  <ion-label floating>Milligram</ion-label>\n   <ion-input type="text" [(ngModel)]="Mil"></ion-input>\n  </ion-item>\n  <ion-item>    \n  <ion-label floating>Frequency</ion-label>\n   <ion-input type="text" [(ngModel)]="Freq"></ion-input>\n  </ion-item>\n\n  <ion-item>\n<ion-label floating>Date</ion-label>\n<ion-datetime displayFormat="MMM/DD/YYYY" [(ngModel)]="myDate"></ion-datetime>\n\n</ion-item>\n\n\n  <!--button to add items to storage-->\n</ion-list>\n<button full ion-button color="danger" (click)="saveItem()">Save</button> \n\n</ion-content>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/medicationadd/medicationadd.html"*/,
    })
    //page class
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], MedicationaddPage);

//# sourceMappingURL=medicationadd.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MedicationviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


/*
component function
*/
var MedicationviewPage = (function () {
    //contructor
    function MedicationviewPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    //data load for the view
    MedicationviewPage.prototype.ionViewDidLoad = function () {
        this.Mname = this.navParams.get('item').Mname;
        this.Mil = this.navParams.get('item').Mil;
        this.myDate = this.navParams.get('item').myDate;
        this.Freq = this.navParams.get('item').Freq;
    };
    return MedicationviewPage;
}());
MedicationviewPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/medicationview/medicationview.html"*/'\n\n  <!--Medication item view page-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title class="opnav">Details</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n <!--Display Medication items list -->\n<ion-content class="monitor">\n  \n      <ion-card>\n  \n          <ion-card-content>\n              <p><b>Medication on :</b> {{myDate}}</p>\n              <br>\n              <p><b>Med. Name :</b> {{Height}}</p>\n                <p><b> Milligram :</b> {{Weight}} MG</p>\n                <p><b> Frequency :</b> {{Freq}}</p>\n          </ion-card-content>\n  \n          <ion-list>\n             \n                 \n \n          </ion-list>\n  \n      </ion-card>\n  \n  </ion-content>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/medicationview/medicationview.html"*/,
    })
    //class for page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], MedicationviewPage);

//# sourceMappingURL=medicationview.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyactProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */



//injectable
var DailyactProvider = (function () {
    //constructor
    function DailyactProvider(storage) {
        this.storage = storage;
    }
    //get data function
    DailyactProvider.prototype.getData = function () {
        return this.storage.get('dailyacts');
    };
    //save data function
    DailyactProvider.prototype.save = function (data) {
        var newData = JSON.stringify(data);
        this.storage.set('dailyacts', newData);
    };
    return DailyactProvider;
}());
DailyactProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], DailyactProvider);

//# sourceMappingURL=dailyact.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WhmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__whmadd_whmadd__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__whmview_whmview__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_todo_todo__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */





//component function
var WhmPage = (function () {
    function WhmPage(navCtrl, navParams, modalCtrl, dataService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.dataService = dataService;
        //arrray for items ansd status
        this.items = [];
        this.dataService.getData().then(function (todos) {
            if (todos) {
                //console.log(todos)
                _this.items = JSON.parse(todos);
            }
        });
    }
    //ionic view load function
    WhmPage.prototype.ionViewDidLoad = function () {
    };
    //add item function
    WhmPage.prototype.addItem = function () {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_1__whmadd_whmadd__["a" /* WhmaddPage */]);
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.saveItem(item);
            }
        });
        addModal.present();
    };
    //save item function
    WhmPage.prototype.saveItem = function (item) {
        this.items.push(item);
        this.dataService.save(this.items);
    };
    //view item function
    WhmPage.prototype.viewItem = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__whmview_whmview__["a" /* WhmviewPage */], {
            item: item
        });
    };
    //delete item function 
    WhmPage.prototype.delete = function (index) {
        this.items.splice(index, 1);
        this.dataService.save(this.items);
    };
    return WhmPage;
}());
WhmPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-whm',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/whm/whm.html"*/'\n<ion-header>\n  \n    <ion-navbar>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Weight and Height Monitor</ion-title>\n          <ion-buttons start>\n            <!--button to call whmadd page or navigate to add page-->\n            <button ion-button color="primary"   id="addmx2" icon-only (click)="addItem()"><ion-icon ios="ios-add" md="md-add" id="addmx"></ion-icon></button>\n           \n            </ion-buttons>\n      </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content>\n       <!--page contents-->\n    <ion-list detail-non>\n         <!--NgFor used to get whm data from the storage and display-->\n      <ion-item-sliding  *ngFor="let item of items; let i = index"(click)="viewItem(item)" detail-none> \n        <ion-item >\n           <ion-icon name= "arrow-forward" item-right></ion-icon>\n        <a>Height : {{item.Height}} & Weight : {{item.Weight}}  on  {{item.myDate}}</a>\n   <!--Delete button to delete items-->\n       </ion-item>\n       <ion-item-options side="left">\n         <button ion-button color="danger" (click)="delete(i)">delete</button>\n       </ion-item-options>\n   \n   \n     </ion-item-sliding>\n     <p>   slide right to delete. click to view item details</p>\n  </ion-list>\n  </ion-content>\n  '/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/whm/whm.html"*/,
    })
    //export class of the page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__providers_todo_todo__["a" /* TodoProvider */]])
], WhmPage);

//# sourceMappingURL=whm.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WhmaddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


var WhmaddPage = (function () {
    //constructor
    function WhmaddPage(navCtrl, navParams, view) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
    }
    //save item function
    WhmaddPage.prototype.saveItem = function () {
        var newItem = {
            myDate: this.myDate,
            Height: this.Height,
            Weight: this.Weight,
        };
        this.view.dismiss(newItem);
    };
    //closs page function
    WhmaddPage.prototype.close = function () {
        this.view.dismiss();
    };
    return WhmaddPage;
}());
WhmaddPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-whmadd',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/whmadd/whmadd.html"*/'\n<!--page for weight and height data enter  -->\n\n<ion-header>\n  \n  <ion-navbar>\n    <ion-title>Data Entry</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-list>\n  <!--page contents\n  -->\n\n    <!--ngModel used to enter data\n  -->\n<ion-item>    \n<ion-label floating>Height</ion-label>\n <ion-input type="number" [(ngModel)]="Height"></ion-input>\n</ion-item>\n<ion-item>    \n  <ion-label floating>Weight</ion-label>\n   <ion-input type="number" [(ngModel)]="Weight"></ion-input>\n  </ion-item>\n\n  <ion-item>\n<ion-label floating>Date</ion-label>\n<ion-datetime displayFormat="MMM/DD/YYYY" [(ngModel)]="myDate"></ion-datetime>\n\n</ion-item>\n\n</ion-list>\n <!--save button to save data to the storage -->\n<button full ion-button color="danger" (click)="saveItem()">Save</button> \n\n</ion-content>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/whmadd/whmadd.html"*/,
    })
    //export class of the page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], WhmaddPage);

//# sourceMappingURL=whmadd.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */



//injectable
var TodoProvider = (function () {
    //contructor
    function TodoProvider(storage) {
        this.storage = storage;
    }
    //get data function
    TodoProvider.prototype.getData = function () {
        return this.storage.get('todos');
    };
    //save data function
    TodoProvider.prototype.save = function (data) {
        var newData = JSON.stringify(data);
        this.storage.set('todos', newData);
    };
    return TodoProvider;
}());
TodoProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], TodoProvider);

//# sourceMappingURL=todo.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__foodadd_foodadd__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__foodv_foodv__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_food_food__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */





/*component calling */
var FoodPage = (function () {
    function FoodPage(navCtrl, navParams, modalCtrl, dataService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.dataService = dataService;
        /*array */
        this.itemsF = [];
        this.dataService.getData().then(function (foods) {
            if (foods) {
                //console.log(todos)
                _this.itemsF = JSON.parse(foods);
            }
        });
    }
    //view data function
    FoodPage.prototype.ionViewDidLoad = function () {
    };
    //add itemn function
    FoodPage.prototype.addItem = function () {
        var _this = this;
        var addModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_1__foodadd_foodadd__["a" /* FoodaddPage */]);
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.saveItem(item);
            }
        });
        addModal.present();
    };
    //save function
    FoodPage.prototype.saveItem = function (item) {
        this.itemsF.push(item);
        this.dataService.save(this.itemsF);
    };
    //view item function
    FoodPage.prototype.viewItem = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__foodv_foodv__["a" /* FoodvPage */], {
            item: item
        });
    };
    //delete items function
    FoodPage.prototype.delete = function (index) {
        this.itemsF.splice(index, 1);
        this.dataService.save(this.itemsF);
    };
    return FoodPage;
}());
FoodPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-food',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/food/food.html"*/'\n<!--\n  Generated template for the NutrientsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  \n    <ion-navbar>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Monitor Dietary</ion-title>\n        <!--button to call add items to dietary monitoring-->\n          <ion-buttons start>\n            <button ion-button color="primary"   id="addmx2" icon-only (click)="addItem()"><ion-icon ios="ios-add" md="md-add" id="addmx"></ion-icon></button>\n           \n            </ion-buttons>\n      </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content>\n        <!--displaying of dietary monitoring item-->\n    <ion-list detail-non>\n      <ion-item-sliding  *ngFor="let item of itemsF; let i = index"(click)="viewItem(item)" detail-none> \n        <ion-item >\n           <ion-icon name= "arrow-forward" item-right></ion-icon>\n        <a>Dietary : {{item.Dname}} Date:  {{item.myDate}} | Time : {{item.Time}}</a>\n   \n       </ion-item>\n       <!--delete item of dietary monitoring item-->\n       <ion-item-options side="left">\n         <button ion-button color="danger" (click)="delete(i)">delete</button>\n       </ion-item-options>\n   \n   \n     </ion-item-sliding>\n     <p>  slide right to delete. click to view item details</p>\n  </ion-list>\n  </ion-content>\n  '/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/food/food.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__providers_food_food__["a" /* FoodProvider */]])
], FoodPage);

//# sourceMappingURL=food.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */



//injectable
var FoodProvider = (function () {
    //constructor
    function FoodProvider(storage) {
        this.storage = storage;
    }
    //get data
    FoodProvider.prototype.getData = function () {
        return this.storage.get('foods');
    };
    //save data
    FoodProvider.prototype.save = function (data) {
        var newData = JSON.stringify(data);
        this.storage.set('foods', newData);
    };
    return FoodProvider;
}());
FoodProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], FoodProvider);

//# sourceMappingURL=food.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/*Ahmed Ismail */
/*import liabraries */

/*Ahmed Ismail */
/*about page ts file*/
var AboutPage = (function () {
    function AboutPage() {
    }
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/about/about.html"*/'\n<!-- About page -->\n<ion-header>\n  <ion-navbar>\n    <ion-title>About</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="cards-bg">\n\n  <ion-card>\n<!--content-->\n\n    <ion-card-content>\n    <ion-list-header class="ggh2">\n     <h2 class="ggh2h" id="img23"> <b>About CysFib App</b></h2>\n     <!--display about app-->\n    </ion-list-header> \n       <img src="assets/img/icon2.png" id="img22">\n       <br>\n      <p id="pp2" >\n        CysFib App is designed and developed to help patients with cystic fibrosis,  especially targeting younger users . This App is provides guidelines, monitoring and other components. \n      </p>\n        <!--display about developer-->\n    <ion-list-header class="ggh2">\n     <h2 class="ggh2h" id="img23"> <b>About Developer</b></h2>\n    </ion-list-header> \n      <img src="assets/img/deve.png" id="img22">\n      <br>\n       <p id="img23"> <b>Ahmed Ismail</b></p>\n       <p id="img23">shinobi.mv@gmail.com</p>\n       <p id="img23">Villa College</p>\n       \n    </ion-card-content>\n\n \n\n  </ion-card>\n\n  \n\n \n\n  \n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/about/about.html"*/
    })
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CysfibPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nutrition_nutrition__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__selfcareguide_selfcareguide__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cfg_cfg__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */






/*component */
var CysfibPage = (function () {
    /*constructor */
    function CysfibPage(navCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        /*slides array */
        this.slides = [
            {
                title: "Nutrient Know-How",
                description: "Like everyone else, guys and girls who have CF should eat a balanced diet that includes plenty of fruits and veggies",
                image: "assets/img/Know-How.jpg",
                io: "load1()",
                po: "1"
            },
            {
                title: "Testing for Cystic fibrosis",
                description: "Diagnosing CF is a multistep process. A complete diagnostic evaluation should include a newborn screening",
                image: "assets/img/digno.png",
                po: "2"
            },
            {
                title: "Diagnosis",
                description: "The significant advances in the diagnosis and treatment of CF over the past decade have increased our ",
                image: "assets/img/diag.png",
                po: "3"
            }
        ];
    }
    /*loadq functon for navigation */
    CysfibPage.prototype.load1 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__nutrition_nutrition__["a" /* NutritionPage */], {
            val: 'Nutrition'
        });
    };
    /*loadSelfguide functon for navigation */
    CysfibPage.prototype.loadSelfcareguide = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__selfcareguide_selfcareguide__["a" /* SelfcareguidePage */], {
            val: 'Selfcareguide'
        });
    };
    /*loadCfg functon for navigation */
    CysfibPage.prototype.loadCfg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cfg_cfg__["a" /* CfgPage */], {
            val: 'Cfg'
        });
    };
    return CysfibPage;
}());
CysfibPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'tutorial-page',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/cysfib/cysfib.html"*/'\n<ion-header>\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n<!--Home page content display\n  -->\n<ion-content class="tutorial-page" id="bbm">\n\n  <ion-slides pager>\n    <ion-slide *ngFor="let slide of slides">\n      <ion-avatar item-start id="ppm1">\n          <img [src]="slide.image">\n        </ion-avatar>\n     \n      <h2 class="ttslide-title" [innerHTML]="slide.title"></h2>\n      <!--ngIf condition used to filter readmore for the display content\n  -->\n      <p [innerHTML]="slide.description"  id="ppm1"> </p>\n      <p *ngIf=" slide.po == \'1\'"><a (click)="load1()">Read More</a></p>\n      <p *ngIf=" slide.po == \'2\'"><a (click)="loadCfg()">Read More</a></p>\n      <p *ngIf=" slide.po == \'3\'"><a (click)="loadSelfcareguide()">Read More</a></p>\n     \n    </ion-slide>\n   \n  </ion-slides>\n</ion-content>'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/cysfib/cysfib.html"*/
    })
    /*page class */
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
], CysfibPage);

//# sourceMappingURL=cysfib.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfcarePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nutrition_nutrition__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__selfcareguide_selfcareguide__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cfg_cfg__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */





var SelfcarePage = (function () {
    //constructor
    function SelfcarePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    //nutrition page nav
    SelfcarePage.prototype.load1 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__nutrition_nutrition__["a" /* NutritionPage */], {
            val: 'Nutrition'
        });
    };
    //selfcare guide page nav
    SelfcarePage.prototype.loadSelfcareguide = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__selfcareguide_selfcareguide__["a" /* SelfcareguidePage */], {
            val: 'Selfcareguide'
        });
    };
    //cf guide page nav
    SelfcarePage.prototype.loadCfg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cfg_cfg__["a" /* CfgPage */], {
            val: 'Cfg'
        });
    };
    return SelfcarePage;
}());
SelfcarePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'self-care',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/self-care/self-care.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title  >\n      Self care\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n     <ion-list>\n       <ion-list-header class="ggh2">\n       <h2 class="ggh2h" > Guidelines </h2>\n    </ion-list-header>\n        <ion-item (click)="loadCfg()">\n    <ion-avatar item-start>\n      <img src="assets/img/cf.png">\n       </ion-avatar>\n        <h2 >CF</h2>  \n        <p class="scfp">Cystic Fibrosis guidelines</p>\n        <button ion-button clear item-end >view</button>\n  </ion-item>\n      </ion-list>\n  <ion-list>\n    \n      <ion-item (click)="load1()">\n        <ion-avatar item-start>\n        <img src="assets/img/nutrition.png">\n    </ion-avatar>\n        <h2>Dietary & Nutrition</h2>  \n        <p class="scfp">Daily dietary & nutrition guidelines</p>\n    <button ion-button clear item-end >view</button>\n  </ion-item>\n</ion-list>\n\n\n\n<ion-list class="scb">\n  <ion-item class="scb" (click)="loadSelfcareguide()">\n    <ion-avatar item-start>\n      <img src="assets/img/icon.png">\n       </ion-avatar>\n        <h2 class="scp" >Cysfib care</h2>  \n        <p class="scpp">Treat your self</p>\n        <button ion-button clear item-end class="scp" >view</button>\n  </ion-item>\n   \n</ion-list>\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/self-care/self-care.html"*/
    })
    //calss of the page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
], SelfcarePage);

//# sourceMappingURL=self-care.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */




/*component */
var DirectoryPage = (function () {
    function DirectoryPage(navCtrl, http, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        /*local data integration */
        var localData = http.get('assets/directory.json').map(function (res) { return res.json().items; });
        localData.subscribe(function (data) {
            _this.information = data;
        });
        console.log(navParams.get('val'));
    }
    /*togglesection functions */
    DirectoryPage.prototype.toggleSection = function (i) {
        this.information[i].open = !this.information[i].open;
    };
    /*toggileItem function */
    DirectoryPage.prototype.toggleItem = function (i, j) {
        this.information[i].children[j].open = !this.information[i].children[j].open;
    };
    /*ionicViewLoad function */
    DirectoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SelfcareguidePage');
    };
    return DirectoryPage;
}());
DirectoryPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-directory',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/directory/directory.html"*/'\n<ion-header>\n  <ion-navbar >\n    <ion-title class="opnav">\n     Directory\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n \n<ion-content>\n\n  <ion-list class="accordion-list">\n      <ion-list-header class="ggh2">\n       <h2 class="ggh2h" > Directory </h2>\n    </ion-list-header>\n    <!-- First Level -->\n    <ion-list-header *ngFor="let item of information; let i = index" no-lines no-padding>\n      <!-- Toggle Button -->\n      <button ion-item (click)="toggleSection(i)" detail-none [ngClass]="{\'section-active\': item.open, \'section\': !item.open}">\n        <ion-icon item-left name="arrow-forward" *ngIf="!item.open"></ion-icon>\n        <ion-icon item-left name="arrow-down" *ngIf="item.open"></ion-icon>\n          {{ item.name }}\n      </button>\n \n      <ion-list *ngIf="item.children && item.open" no-lines>\n        <!-- Second Level -->\n        <ion-list-header *ngFor="let child of item.children; let j = index" no-padding>\n          <!-- Toggle Button -->\n          <button ion-item (click)="toggleItem(i, j)" *ngIf="child.children" class="child" detail-none>\n            <ion-icon item-left name="add" *ngIf="!child.open"></ion-icon>\n            <ion-icon item-left name="close" *ngIf="child.open"></ion-icon>\n            {{ child.name }}\n          </button>\n \n          <!-- Direct Add Button as Fallback -->\n          <ion-item *ngIf="!child.children" ion-item detail-none class="child-item" text-wrap>\n          \n            \n            <p class="brcc" text-lowercase>{{ child.information }}</p>\n            \n          </ion-item>\n \n          <ion-list *ngIf="child.children && child.open">\n            <!-- Third Level -->\n            <ion-item *ngFor="let item of child.children; let k = index" detail-none class="child-item" text-wrap>\n            \n              <p *ngIf=" item.cat == \'1\'" ><b> Address:</b> {{ item.information }}</p>\n              <p  *ngIf=" item.cat == \'1\'"  ><b> Open Hours:</b> {{ item.op }}</p>              \n            \n              <p  *ngIf=" item.cat == \'2\'"  ><b> Qualification:</b> {{ item.qf }}</p>\n              <p  *ngIf=" item.cat == \'2\'"  ><b> Work Place:</b> {{ item.wp }}</p>\n              <p  *ngIf=" item.cat == \'2\'"  ><b> Department:</b> {{ item.dep }}</p>\n              <p   ><b> Phone:</b> {{ item.phone }}</p>\n              <!-- Direct Add Button -->\n              \n            </ion-item>\n            \n          </ion-list>\n \n        </ion-list-header>\n      </ion-list>\n      \n    </ion-list-header>\n  </ion-list>\n  \n\n</ion-content>\n\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/directory/directory.html"*/
    })
    /*page class */
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* NavParams */]])
], DirectoryPage);

//# sourceMappingURL=directory.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(237);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_healthcal_healthcal__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_monitoring_monitoring__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_cysfib_cysfib__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_about_about__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_self_care_self_care__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_directory_directory__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_nutrition_nutrition__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_selfcareguide_selfcareguide__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_details_details__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_medication_medication__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_cfg_cfg__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_medicationadd_medicationadd__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_add_add__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_storage__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_whmadd_whmadd__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_whmview_whmview__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__angular_http__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_todo_todo__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_whm_whm__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_dailyact_dailyact__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_medicationview_medicationview__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_food_food__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_foodadd_foodadd__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_foodv_foodv__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_food_food__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/*Ahmed Ismail */
/*import liabraries for the application */
































//NgModule declaration
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_monitoring_monitoring__["a" /* MonitoringPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_cysfib_cysfib__["a" /* CysfibPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_self_care_self_care__["a" /* SelfcarePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_directory_directory__["a" /* DirectoryPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_nutrition_nutrition__["a" /* NutritionPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_selfcareguide_selfcareguide__["a" /* SelfcareguidePage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_cfg_cfg__["a" /* CfgPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_details_details__["a" /* DetailsPage */],
            __WEBPACK_IMPORTED_MODULE_4__pages_healthcal_healthcal__["a" /* HealthcalPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_medication_medication__["a" /* MedicationPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_medicationadd_medicationadd__["a" /* MedicationaddPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_add_add__["a" /* AddPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_whm_whm__["a" /* WhmPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_whmview_whmview__["a" /* WhmviewPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_whmadd_whmadd__["a" /* WhmaddPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_medicationview_medicationview__["a" /* MedicationviewPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_food_food__["a" /* FoodPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_foodv_foodv__["a" /* FoodvPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_foodadd_foodadd__["a" /* FoodaddPage */]
        ],
        //imports
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_23__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_18__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/selfcareguide/selfcareguide.module#SelfcareguidePageModule', name: 'SelfcareguidePage', segment: 'selfcareguide', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/whmview/whmview.module#WhmviewPageModule', name: 'WhmviewPage', segment: 'whmview', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/foodv/foodv.module#FoodvPageModule', name: 'FoodvPage', segment: 'foodv', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/foodadd/foodadd.module#FoodaddPageModule', name: 'FoodaddPage', segment: 'foodadd', priority: 'low', defaultHistory: [] }
                ]
            })
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
        //components for import
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_monitoring_monitoring__["a" /* MonitoringPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_cysfib_cysfib__["a" /* CysfibPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_self_care_self_care__["a" /* SelfcarePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_directory_directory__["a" /* DirectoryPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_nutrition_nutrition__["a" /* NutritionPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_selfcareguide_selfcareguide__["a" /* SelfcareguidePage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_cfg_cfg__["a" /* CfgPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_details_details__["a" /* DetailsPage */],
            __WEBPACK_IMPORTED_MODULE_4__pages_healthcal_healthcal__["a" /* HealthcalPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_medication_medication__["a" /* MedicationPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_medicationadd_medicationadd__["a" /* MedicationaddPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_add_add__["a" /* AddPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_whm_whm__["a" /* WhmPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_whmview_whmview__["a" /* WhmviewPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_whmadd_whmadd__["a" /* WhmaddPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_medicationview_medicationview__["a" /* MedicationviewPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_food_food__["a" /* FoodPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_foodv_foodv__["a" /* FoodvPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_foodadd_foodadd__["a" /* FoodaddPage */]
        ],
        //including providers
        providers: [
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_24__providers_todo_todo__["a" /* TodoProvider */],
            __WEBPACK_IMPORTED_MODULE_26__providers_dailyact_dailyact__["a" /* DailyactProvider */],
            __WEBPACK_IMPORTED_MODULE_26__providers_dailyact_dailyact__["a" /* DailyactProvider */],
            __WEBPACK_IMPORTED_MODULE_28__providers_food_food__["a" /* FoodProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */


/*Add data page*/
var AddPage = (function () {
    function AddPage(nav) {
        this.nav = nav;
        this.todoList = JSON.parse(localStorage.getItem("todos"));
        if (!this.todoList) {
            this.todoList = [];
        }
        this.todoItem = "";
    }
    /*save function*/
    AddPage.prototype.save = function () {
        if (this.todoItem != "") {
            this.todoList.push(this.todoItem);
            localStorage.setItem("todos", JSON.stringify(this.todoList));
            this.nav.pop();
        }
    };
    return AddPage;
}());
AddPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/add/add.html"*/'\n\n  <!--add for items in guidlines\n  -->\n<ion-header>\n    <ion-navbar>\n        <ion-title>Add guidlines</ion-title>\n        <ion-buttons end>\n            <button (click)="save()"><ion-icon name="checkmark"></ion-icon></button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n  <!--add items to guidlines\n  --> \n<ion-content class="add">\n    <ion-list>\n        <ion-item>\n              <!-- input items to guidlines\n  --> \n            <ion-label floating>Todo Item</ion-label>\n            <ion-input type="text" [(ngModel)]="todoItem"></ion-input>\n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/add/add.html"*/
    })
    /*Add page class*/
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
], AddPage);

//# sourceMappingURL=add.js.map

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelfcareguidePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */




//component fucntion
var SelfcareguidePage = (function () {
    function SelfcareguidePage(navCtrl, http, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        //getting localdata from json file 
        var localData = http.get('assets/information.json').map(function (res) { return res.json().items; });
        localData.subscribe(function (data) {
            _this.information = data;
        });
        console.log(navParams.get('val'));
    }
    //toggleswection
    SelfcareguidePage.prototype.toggleSection = function (i) {
        this.information[i].open = !this.information[i].open;
    };
    //toggleItem
    SelfcareguidePage.prototype.toggleItem = function (i, j) {
        this.information[i].children[j].open = !this.information[i].children[j].open;
    };
    //toggledidload function
    SelfcareguidePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SelfcareguidePage');
    };
    return SelfcareguidePage;
}());
SelfcareguidePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["d" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/selfcareguide/selfcareguide.html"*/'\n\n<ion-header>\n  <ion-navbar >\n    <ion-title class="opnav">\n      Cysfib Care\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n \n<ion-content>\n\n  <ion-list class="accordion-list">\n      <ion-list-header class="ggh2">\n       <h2 class="ggh2h" > Cysfib-care Guides </h2>\n    </ion-list-header>\n    <!-- First Level -->\n    <ion-list-header *ngFor="let item of information; let i = index" no-lines no-padding>\n      <!-- Toggle Button -->\n      <button ion-item (click)="toggleSection(i)" detail-none [ngClass]="{\'section-active\': item.open, \'section\': !item.open}">\n        <ion-icon item-left name="arrow-forward" *ngIf="!item.open"></ion-icon>\n        <ion-icon item-left name="arrow-down" *ngIf="item.open"></ion-icon>\n          {{ item.name }}\n      </button>\n \n      <ion-list *ngIf="item.children && item.open" no-lines>\n        <!-- Second Level -->\n        <ion-list-header *ngFor="let child of item.children; let j = index" no-padding>\n          <!-- Toggle Button -->\n          <button ion-item (click)="toggleItem(i, j)" *ngIf="child.children" class="child" detail-none>\n            <ion-icon item-left name="add" *ngIf="!child.open"></ion-icon>\n            <ion-icon item-left name="close" *ngIf="child.open"></ion-icon>\n            {{ child.name }}\n          </button>\n \n          <!-- Direct Add Button as Fallback -->\n          <ion-item *ngIf="!child.children" ion-item detail-none class="child-item" text-wrap>\n          \n            \n            <p class="brcc" text-lowercase>{{ child.information }}</p>\n            \n          </ion-item>\n \n          <ion-list *ngIf="child.children && child.open">\n            <!-- Third Level -->\n            <ion-item *ngFor="let item of child.children; let k = index" detail-none class="child-item" text-wrap>\n            \n              <p class="brcc" >{{ item.information }}</p>\n              <!-- Direct Add Button -->\n              \n            </ion-item>\n            \n          </ion-list>\n \n        </ion-list-header>\n      </ion-list>\n      \n    </ion-list-header>\n  </ion-list>\n  \n\n</ion-content>\n\n'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/selfcareguide/selfcareguide.html"*/,
    })
    //class of the page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* NavParams */]])
], SelfcareguidePage);

//# sourceMappingURL=selfcareguide.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NutritionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__details_details__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */



var NutritionPage = (function () {
    function NutritionPage(navCtrl) {
        this.navCtrl = navCtrl;
        //list data 
        this.list = [
            { Name: 'Nutrient Know-How', Design: 'Like everyone else, guys and girls who have CF should eat a balanced diet that includes plenty of fruits and veggies, whole grains, dairy products, and protein. In addition, people with CF have some specific nutritional needs to help them stay healthy. Here are some of the nutrients they need to get more of:', Image: 'Know-How.jpg' },
            { Name: 'Important Facts', Design: 'Since the teen years are a time of rapid growth, a balanced diet, which includes optimal calories, vitamins and minerals, is important for good nutritional health for any teen with EPI due to CF. Remember that teens with CF need more calories, vitamins and salt than teens who do not have CF. Even if teens eat a diet that is well-balanced, they may also need to take additional vitamin supplements in order to maintain normal vitamin levels', Image: 'teen.jpg' },
            { Name: 'Meals and Munchies', Design: 'Peoples  with CF may need to eat more regularly than some of their friends do, but that doesn t mean they have to eat differently. Check out this sample meal plan that provides approximately 3,750 calories. The links to some of these foods will give you recipes for high-calorie foods that teens with CF can make: \n\n \u2022 Breakfast — 3 frozen pancakes with 1 tablespoon butter and 3 tablespoons syrup; ½ cup of strawberries; Mighty Milk, \n\n\u2022 Morning snack — 1 cup whole-milk yogurt; ½ cup granola; ½ banana; water, \n\n\u2022 Lunch — Sandwich made with 4 ounces of turkey, 1 ounce of cheese, 1 tablespoon mayonnaise, 3 teaspoons mustard, lettuce, tomato, and/or onion; 10 baby carrots with 2 tablespoons ranch dressing; one apple; 14 pretzels; water, \n\n\u2022 Afternoon snack — ½ cup trail mix and 1 cup Mighty Milk \n\n\u2022 Dinner — Creamy chicken fettuccine with broccoli; water', Image: 'meal.jpg' },
        ];
    }
    //list item function
    NutritionPage.prototype.Listitem = function (l) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__details_details__["a" /* DetailsPage */], { item: l });
    };
    return NutritionPage;
}());
NutritionPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'nutrition',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/nutrition/nutrition.html"*/'<ion-header>\n  <ion-navbar color="apphead"  >\n    <ion-title class="opnav">\n      Nutrition and Dietary Guide\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list-header class="ggh2">\n       <h2 class="ggh2h" > Guides </h2>\n    </ion-list-header>\n  <ion-list >\n    <ion-item *ngFor="let l of list" (click)="Listitem(l)">\n     \n      <h2>{{l.Name}}</h2>\n      <p class="mpline">{{l.Design}}</p>\n      <ion-icon clear item-right name="arrow-forward" class="debutton"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/nutrition/nutrition.html"*/
    })
    //class of the page
    ,
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
], NutritionPage);

//# sourceMappingURL=nutrition.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CfgPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__details_details__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*Ahmed Ismail */
/*import liabraries */



/*components*/
var CfgPage = (function () {
    function CfgPage(navCtrl) {
        this.navCtrl = navCtrl;
        /*list*/
        this.list = [
            { Name: 'About Cystic Fibrosis', Design: 'Learn about cystic fibrosis, a genetic lung disorder that affects the pancreas and other organs, and how to treat and live with this chronic disease.', Image: 'cys-kid.png' },
            { Name: 'Diagnosis', Design: 'The significant advances in the diagnosis and treatment of CF over the past decade have increased our understanding of the disease, making this an opportune time to reexamine the criteria for a diagnosis of CF. For example, the age of onset of symptoms is increasingly recognized as being highly variable, ranging from prenatal evidence of echogenic bowel to onset of symptoms in late adolescence or adulthood that nevertheless can cause major morbidity and premature mortality.', Image: 'diag.png' },
            { Name: 'Testing for Cystic fibrosis', Design: 'Diagnosing CF is a multistep process. A complete diagnostic evaluation should include a newborn screening, a sweat chloride test, a genetic or carrier test and a clinical evaluation at a CF Foundation-accredited care center.', Image: 'digno.png' }
        ];
    }
    /*list item function*/
    CfgPage.prototype.Listitem = function (l) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__details_details__["a" /* DetailsPage */], { item: l });
    };
    return CfgPage;
}());
CfgPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'cfg',template:/*ion-inline-start:"/Users/isse/dddapp/Cysfib/src/pages/cfg/cfg.html"*/'\n<ion-header>\n  <ion-navbar color="apphead"  >\n    <ion-title class="opnav">\n      CF Guide\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<!--Cystic Fibrosis guidlines \n  -->\n<ion-content padding>\n  <ion-list-header class="ggh2">\n       <h2 class="ggh2h" > CF Guides </h2>\n    </ion-list-header>\n  <ion-list >\n    <!--ngIF used to get items from the datastore\n  -->\n    <ion-item *ngFor="let l of list" (click)="Listitem(l)">\n     \n      <h2>{{l.Name}}</h2>\n      <p class="mpline">{{l.Design}}</p>\n      <ion-icon clear item-right name="arrow-forward" class="debutton"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/isse/dddapp/Cysfib/src/pages/cfg/cfg.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
], CfgPage);

//# sourceMappingURL=cfg.js.map

/***/ })

},[218]);
//# sourceMappingURL=main.js.map